# -*- coding: utf-8 -*-

from django.db import models
from core.models import ExtensionUser
from django.conf import settings
from datetime import datetime, time 

# Create your models here.

class TypeSurvay(models.Model):
	"""
	вид обследования (МРТ, КТ и т.д.)
	"""
	class Meta:
		db_table = 'type_survay'
		verbose_name = 'Вид исследования'
		verbose_name_plural = 'Виды исследований'
	GROUP_TYPE = (
		(0, 'общие обследования'),
		(1, 'дорогостоящие обследования'),
	)
	name = models.CharField(max_length=100)
	caption = models.CharField(max_length=150, help_text="название, отображаемое в печатных бланках")
	group = models.PositiveSmallIntegerField(choices=GROUP_TYPE, default=0)

	def __str__(self):
		return self.name

	def __repr__(self):
		return "{}({})".format(self.name, self.caption)

	def __format__(self):
		return self.caption

	def get_group(self):
		return GROUP_TYPE[self.group][1]

class Survay(models.Model):
	"""
	обследования и квоты
	"""
	class Meta:
		db_table = 'survay'
		verbose_name = 'Исследование'
		verbose_name_plural = 'Исследования'

	name = models.CharField(max_length=255)
	group = models.ForeignKey(TypeSurvay)
	unlock_count = models.PositiveIntegerField(default=0, help_text="кол-во доступных квот")
	lock_count = models.PositiveIntegerField(default=0, help_text="кол-во зарезервированных квот")
	max_limit = models.PositiveIntegerField(default=10, help_text="верхний порог резерва квот")
	min_limit = models.PositiveIntegerField(default=0, help_text="нижний порог резерва квот")

	def __str__(self):
		return '{} {}'.format(self.group.caption, self.name)
		#return self.name

	def __format__(self):
		return '{} {}'.format(self.group.caption, self.name)

	def add_quota(self, count):
		delta = self.max_limit - self.lock_count
		if delta > 0:
			if count <= delta:
				self.lock_count += count
			else:
				rest = count - delta
				self.lock_count = self.max_limit
				self.unlock_count += rest
		self.save(update_fields=['unlock_count', 'lock_count'])

	def short_serializing(self):
		return dict(
						id = self.id,
						name = self.name
					)

class MedicHomeSurvay(models.Model):
	class Meta:
		db_table = 'medic_home'
		verbose_name = 'Мед.организация(услуга)'
		verbose_name_plural = 'Мед.организации(услуги)'

	name = models.CharField(max_length=155, help_text='название мед. учреждения')
	survay = models.ForeignKey(Survay)
	address = models.CharField(max_length=255)
	count_quot = models.PositiveIntegerField(default=0)

	def __str__(self):
		return self.name + ': ' + str(self.survay)
		#return '{}: {}'.format(self.name, self.survay)

	def add_quota(self, count):
		self.survay.add_quota(count)


class Patient(models.Model):
	"""
	пациенты
	"""
	class Meta:
		db_table = 'patient'
		verbose_name = 'Пациент'
		verbose_name_plural = "Пациенты"
	oms = models.CharField(max_length=16, unique=True)
	name = models.CharField(max_length=100)
	sur_name = models.CharField(max_length=100)
	last_name = models.CharField(max_length=100, blank=True, null=True)
	birthday = models.DateField()
	address = models.CharField(max_length=200)
	passport = models.CharField(max_length=10)
	kontakt = models.CharField(max_length=20)
	email = models.CharField(max_length=100, blank=True, null=True, 
							help_text="на емайл может высылаться уведомление об обследовании по желанию")
	allow_newsletter = models.BooleanField(default=False, help_text="разрешить рассылку на е-майл")

	def __str__(self):
		return '{} {}.'.format(self.sur_name, self.name[0])

	def __format__(self):
		return '{} {}.'.format(self.sur_name, self.name[0])

	def short_serializing(self):
		fio = '{} {} {}'.format(self.sur_name, self.name, self.last_name)
		return dict(
					id = self.id,
					fio = fio,
					oms = self.oms,
				)

	def for_serializing(self):
		fio = '{} {} {}'.format(self.sur_name, self.name, self.last_name)
		return dict(
					id = self.id,
					fio = fio,
					oms = self.oms,
					birthday = datetime.strftime(self.birthday, '%d.%m.%Y'),
					address = self.address,
					passport_ser = self.passport[:4],
					passport_num = self.passport[4:],
					kontakt = self.kontakt,
					email = self.email,
					newsletter = 'есть' if self.allow_newsletter else 'нет'
				)

class Diagn(models.Model):
	"""
	Дигнозы
	"""
	class Meta:
		db_table = 'diagnos'
		verbose_name = 'Диагноз'
		verbose_name_plural = 'МКБ диагнозы'
	code = models.CharField(max_length=10)
	about = models.CharField(max_length=255)

	def __str__(self):
		return self.code

class Records(models.Model):
	"""
	записи в лист ожидания
	"""
	class Meta:
		db_table = 'records'
		verbose_name = 'Запись на обследование'
		verbose_name_plural = 'Запись на обследования'
	STATUS = (
		(0, 'ожидание'),
		(1, 'утверждено'),
		(2, 'пройдено'),
		(3, 'отказано'),
		(4, 'неявка'),
	)
	STYLE = (
		(0, 'success'),
		(1, 'success'),
		(2, ''),
		(3, 'danger'),
		(4, 'danger'),
	)
	NOTICE = (
		(0, 'нет'),
		(0, 'попытка уведомления не удалась'),
		(0, 'пациент уведомлен'),
	)
	patient = models.ForeignKey(Patient)
	survay = models.ForeignKey(Survay)
	created_at = models.DateTimeField(auto_now_add=True, help_text="дата заявки")
	diagn = models.ForeignKey(Diagn)
	justification = models.CharField(max_length=255, default="", help_text="обоснование")
	last_action = models.DateTimeField(auto_now=True, help_text="дата последней обработки заявки")
	status = models.PositiveSmallIntegerField(choices=STATUS, default=0)
	date_rec = models.DateField(null=True, blank=True, help_text="дата на которую назначена запись")
	time_rec = models.TimeField(null=True, blank=True, help_text="время на которое назначена запись")
	doctor = models.ForeignKey(ExtensionUser)
	notice_call = models.PositiveSmallIntegerField(choices=NOTICE, default=0, help_text="уведомление пациента о записи")
	notice_email = models.PositiveSmallIntegerField(choices=NOTICE, default=0, help_text="уведомление пациента по e-mail")
	medhome = models.ForeignKey(MedicHomeSurvay, null=True, blank=True)
	urgent = models.BooleanField(default=False, help_text="срочно!")

	def __str__(self):
		return 'REC: {}({})'.format(self.id, self.patient.sur_name)

	def get_status(self):
		return self.STATUS[self.status][1]

	def get_style(self):
		return self.STYLE[self.status][1]

	def short_serializing(self):
		return dict(
					survay = str(self.survay),
					created_at = datetime.strftime(self.created_at, '%d.%m.%Y'),
					status = self.STATUS[self.status][1],
					status_id = self.status,
					id = self.id
				)
	
	def for_serializing(self):
		return dict(
						survay = str(self.survay),
						created_at = datetime.strftime(self.created_at, '%d.%m.%Y'),
						status = self.STATUS[self.status][1],
						id = self.id,
						doctor = str(self.doctor),
						diagn = str(self.diagn),
						about = self.justification,
						daterec = datetime.strftime(self.date_rec, '%d.%m.%Y') if self.date_rec \
													else 'не назначено',
						timerec = str(self.time_rec) if self.time_rec else '-',
						medhome = self.medhome.name if self.medhome else 'не назначено',
						notice_call = 'уведомлен звонком' if self.notice_call \
													else 'нет уведомления (call-центр)',
						notice_email = 'уведомление по электронной почте' if self.notice_email \
													else 'нет уведомления (e-mail)'
					)

	def for_all_records(self):
		return dict(
						survay = str(self.survay),
						created_at = datetime.strftime(self.created_at, '%d.%m.%Y'),
						status = self.STATUS[self.status][1],
						id = self.id,
						doctor = str(self.doctor),
						diagn = str(self.diagn),
						about = self.justification,
						daterec = datetime.strftime(self.date_rec, '%d.%m.%Y') if self.date_rec \
													else 'не назначено',
						timerec = str(self.time_rec) if self.time_rec else '-',
						medhome = self.medhome.name if self.medhome else 'не назначено',
						notice_call = 'уведомлен звонком' if self.notice_call \
													else 'нет уведомлений (call-центр)',
						notice_email = 'уведомление по электронной почте' if self.notice_email \
													else 'нет уведомлений (e-mail)',
						patient = '{} {} {}'.format(self.patient.sur_name, self.patient.name,
													self.patient.last_name),
						last_action = datetime.strftime(self.last_action, '%d.%m.%Y'),
						urgent = self.urgent,
						style = self.get_style()
					)





	
