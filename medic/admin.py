from django.contrib import admin
from medic.models import *

# Register your models here.

@admin.register(TypeSurvay)
class TypeSurvayAdmin(admin.ModelAdmin):
	list_display = ('name', 'caption', 'group', )
	list_filter = ('group', )

@admin.register(Survay)
class SurvayAdmin(admin.ModelAdmin):
	list_display = ('name', 'group', 'unlock_count', 'lock_count', 'min_limit', 'max_limit', )
	list_filter = ('group', )

@admin.register(MedicHomeSurvay)
class MedicHomeAdmin(admin.ModelAdmin):
	list_display = ('name', 'survay', 'address', 'count_quot', )
	list_filter = ('name', )

@admin.register(Patient)
class PatientAdmin(admin.ModelAdmin):
	list_display = ('oms', 'sur_name', 'name', 'last_name', 'birthday', 'address',
				'passport', 'kontakt', 'email', 'allow_newsletter')
	search_fields = ('oms', 'sur_name', 'name')

@admin.register(Diagn)
class DiagnAdmin(admin.ModelAdmin):
	list_display = ('code', 'about', )
	search_fields = ('about', 'code')

@admin.register(Records)
class RecordsAdmin(admin.ModelAdmin):
	list_display = ('patient', 'survay', 'created_at', 'diagn', 'justification', 'last_action',
					'status', 'date_rec', 'time_rec', 'doctor', 'notice_call', 'notice_email')

