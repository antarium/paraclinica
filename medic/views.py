from django.shortcuts import render, redirect
from django.views.generic import TemplateView, View, ListView
from django.conf import settings
from django.http import HttpResponse
from django.db.models import Q
from .models import Patient, Records, Diagn, TypeSurvay, Survay, MedicHomeSurvay
from paraclinika.mixins import ListingRecords
import json, datetime

# Create your views here.
class RecordPatient(TemplateView):
	template_name = 'medic/rec_patient.html'

	def get_context_data(self, **kwargs):
		context = super(RecordPatient, self).get_context_data(**kwargs)
		context['diagn_list'] = Diagn.objects.all()
		context['type_survay'] = TypeSurvay.objects.all()
		return context

class FindPatient(View):
	""" 
	получаем омс из веб-формы, и фильтруем базу
	"""
	def post(self, request, *args, **kwargs):
		oms = request.POST.get('oms', None)
		if oms:
			pat_list = Patient.objects.filter(oms__icontains=oms)
			if not pat_list:
				return HttpResponse(json.dumps(''), status=200)
			pat_list = [item.short_serializing() for item in pat_list]
			return HttpResponse(json.dumps(pat_list), status=200)
		return HttpResponse(json.dumps(''), status=200)

class CurrentPatient(View): 
	"""
	вызывается при открытии карты пациента. собирает данные о выбранном пациенте
	"""

	def post(self, request, *args, **kwargs):
		patient = Patient.objects.filter(id=request.POST.get('id', 0)).first()
		if patient:
			pat = patient.for_serializing()
			records= Records.objects.filter(patient=patient).order_by('-created_at')
			records = [item.short_serializing() for item in records]
			out = dict(info=pat, records=records)
			return HttpResponse(json.dumps(out), status=200)
		return HttpResponse(json.dumps(''), status=200)

class GetSurvayList(View):

	def post(self, request, *args, **kwargs):
		survay_list = Survay.objects.filter(group_id=request.POST.get('type_survay'))
		survay_list = [item.short_serializing() for item in survay_list]
		return HttpResponse(json.dumps(survay_list), status=200)

class CheckSurvay(View):

	def post(self, request, *args, **kwargs):
		if request.POST.get('patient', None):
			last_rec = Records.objects.filter(patient_id=request.POST['patient'], 
							survay_id=request.POST.get('survay', 0)).order_by('-created_at').first()
			out = last_rec.short_serializing() if last_rec else ''
			#import ipdb; ipdb.set_trace()
			return HttpResponse(json.dumps(out), status=200)
		return HttpResponse(json.dumps(''), status=200)

class GetFullRecordInfo(View):

	def post(self, request, *args, **kwargs):
		if request.POST.get('record_id', None):
			rec = Records.objects.filter(id=request.POST['record_id']).first()
			if rec:
				return HttpResponse(json.dumps(rec.for_serializing()), status=200)
		return HttpResponse(json.dumps(''), status=200)

class ChangePatient(View):
	new_patient = False

	def post(self, request, *args, **kwargs):
		patient = Patient.objects.filter(Q(oms=request.POST.get('oms', None)) | 
											Q(passport=request.POST.get('passport', None))).first()
		if patient:
			if self.new_patient:
				code = 409
			else:
				code = 200
				patient.oms = request.POST.get('oms', None)
				patient.name = request.POST.get('name', None)
				patient.sur_name = request.POST.get('sur_name', None)
				patient.last_name = request.POST.get('last_name', None)
				patient.address = request.POST.get('address', None)
				patient.passport = request.POST.get('passport', None)
				patient.kontakt = request.POST.get('kontakt', None)
				patient.email = request.POST.get('email', None)
				patient.allow_newsletter = True if request.POST.get('allow_newsletter', None) else False
				patient.save()
			return HttpResponse(patient.id, status=code)

		if self.new_patient:
			fio = request.POST.get('fio', '')
			if len(fio.split(' ')) >= 2 :
				if len(fio.split(' ')) == 2 :
					name, sur_name = fio.split(' ')
				else:
					name, sur_name, last_name = fio.split(' ')[:3]
				patient = Patient(
									oms = request.POST.get('oms', None),
									name = name,
									sur_name = sur_name,
									last_name = last_name,
									birthday = datetime.datetime.strptime(request.POST['birthday'], '%Y-%m-%d'),
									address = request.POST.get('address', None),
									passport = request.POST.get('passport', None),
									kontakt = request.POST.get('kontakt', None),
									email = request.POST.get('email', None),
									allow_newsletter = True if request.POST.get('allow_newsletter', None) else False
								)
				patient.save()
				return HttpResponse(patient.id, status=200)
			else:
				return HttpResponse('ФИО', status=400)
		return HttpResponse(1, status=200)
		

class AllRecords(ListingRecords): 
	"""
	поиск записей на обследования, в т.ч по токену
	"""
	template_name = 'medic/records.html'
	model = Records
	context_object_name = 'records_list'
	paginate_by = settings.COUNT_PATIENT_RECORDS

	def get_context_data(self, *args, **kwargs):
		context = super(AllRecords, self).get_context_data(*args, **kwargs)
		context['diagn_list'] = Diagn.objects.all()
		context['type_survay'] = TypeSurvay.objects.all()
		return context


class ApprovRecord(View):
	"""
	утвердить запись
	"""

	def post(self, request, *args, **kwargs):
		date = datetime.datetime.strptime('{}-{}'.format(request.POST['date_rec'], 
											request.POST['time_rec']), '%Y-%m-%d-%H:%M')
		record = Records.objects.filter(id=request.POST.get('rec_id', 0)).first()
		if record:
			record.date_rec = date.date()
			record.time_rec = date.time()
			record.medhome_id = int(request.POST.get('medhome', 0))
			record.status = 1
			record.save(update_fields=['date_rec', 'time_rec', 'medhome', 'status'])
		return HttpResponse(record.id, status=200)


class GetMedhomeList(View):
	"""
	получить по обследованию список мед, учреждений, куда можно отправить
	с этим обследованием
	"""

	def post(self, request, *args, **kwargs):
		record = Records.objects.filter(id=request.POST.get('record', 0)).first()
		med_list = MedicHomeSurvay.objects.filter(survay=record.survay).values('id', 
													'name', 'address', 'count_quot')
		return HttpResponse(json.dumps([item for item in med_list]), status=200)


class NewRecord(View):
	"""
	записать пациента на новое обследование
	"""
	def post(self, request, *args, **kwargs):
		if request.POST.get('patient_id', None):
			new_rec = Records(
								patient_id = request.POST['patient_id'],
								survay_id = request.POST['survay'],
								diagn_id = request.POST['diagn'],
								justification = request.POST['about-rec'],
								doctor = request.user.extensionuser,
								urgent = True if request.POST.get('promptly-rec', None) else False
							)
			new_rec.save()
			records = [new_rec.short_serializing()]
			return HttpResponse(json.dumps(records), status=200)
		return HttpResponse('', status=400)


class FilterListRecord(View):
	"""
	отфильтровать лист направлений пациента по значению filter на страинце Запись пациента
	"""

	def post(self, request, *args, **kwargs):
		if request.POST.get('patient_id', None):
			filter_param = request.POST.get('filter', 'all')
			if filter_param == 'all':
				recs = Records.objects.filter(patient_id=request.POST['patient_id'])
			elif filter_param == 'opened':
				recs = Records.objects.filter(patient_id=request.POST['patient_id'],
											status__in= [0, 1])
			elif filter_param == 'closed':
				recs = Records.objects.filter(patient_id=request.POST['patient_id'],
											status__in= [2, 3])
			records = [rec.short_serializing() for rec in recs]
			return HttpResponse(json.dumps(records), status=200)
		return HttpResponse('', status=400)




