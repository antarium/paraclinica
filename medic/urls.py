from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from medic.views import *


#import api_login_required
urlpatterns = [
	url(r'^rec/find/', FindPatient.as_view(), name='find_patient'),
	url(r'^rec/patient/change/', ChangePatient.as_view(), name='change_patient'),
	url(r'^rec/patient/new/', ChangePatient.as_view(new_patient=True), name='new_patient'),
	url(r'^rec/patient/new_record/', NewRecord.as_view(), name='new_record'),
	url(r'^rec/patient/', CurrentPatient.as_view(), name='current_patient'),
	url(r'^rec/survay_list/', GetSurvayList.as_view(), name='survay_list'),
	url(r'^rec/check_survay/', CheckSurvay.as_view(), name='check_survay'),
	url(r'^rec/record_full_info/', GetFullRecordInfo.as_view(), name='record_full_info'),
	url(r'^rec/record_filter/', FilterListRecord.as_view(), name='record_filter'),
	url(r'^rec/', RecordPatient.as_view(), name='rec'),
	url(r'^all_rec/filter/', AllRecords.as_view(), name='filtering_records'),
	url(r'^all_rec/aprove/', ApprovRecord.as_view(), name='aprove_record'),
	url(r'^all_rec/record/medhome_list/', GetMedhomeList.as_view(), name='medhome_list'),
	url(r'^all_rec/', AllRecords.as_view(), name='all_records'),
]