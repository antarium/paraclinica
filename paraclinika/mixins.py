from django.conf import settings
from django.views.generic import ListView
from django.http import HttpResponse
from django.db.models import Q
import datetime
import json

class ListingView(ListView): 
	"""
	дополняет стандартный класс ListView
	"""

	def get_page_numbers(self, page_obj):
		"""
		получаем номера страниц для панели пагинации в шаблон
		"""
		min_num = page_obj.number-settings.INTS_PAGE_NUMBERS
		max_num = page_obj.number+settings.INTS_PAGE_NUMBERS
		valid_numbers = [num for num in range(min_num, max_num) if num > 0]
		all_numbers = [num for num in page_obj.paginator.page_range if num in valid_numbers]
		return all_numbers

	def get_context_data(self, **kwargs):
	    context = super(ListingView, self).get_context_data(**kwargs)
	    if context.get('is_paginated', None):
	    	context['page_numbers'] = self.get_page_numbers(context['page_obj'])
	    return context

class ListingRecords(ListingView):

	def post(self, request, *args, **kwargs):
		qs = self.get_queryset()
		qs_list = [item.for_all_records() for item in qs]
		len_qs = len(qs_list)
		count_pages = len_qs // self.paginate_by
		if len_qs % self.paginate_by:
			count_pages+=1
		return HttpResponse(json.dumps(dict(
							records=qs_list, 
							pag=dict(records=len_qs,
									 paginate_by=self.paginate_by,
									 pages=count_pages,
									 num_page=request.POST.get('page', 1)))), status=200)

	def search_by_patient(self):
		text = self.request.POST.get('search_text', '');
		qs = self.queryset.filter(Q(patient__name__icontains=text) | 
									Q(patient__sur_name__icontains=text) |
									Q(patient__last_name__icontains=text))
		return qs

	def filter_by(self):
		"""
		получаю из POST данные для фильтрации и фильтрую QuerySet
		"""
		params = dict()
		if int(self.request.POST.get('survay', None)):
			params.update(survay_id=int(self.request.POST['survay']))
		elif self.request.POST.get('type-survay', None) and self.request.POST['type-survay'] != '0':
			params.update(survay__group_id=int(self.request.POST['type-survay']))
		if self.request.POST.get('diagn', None):
			params.update(diagn_id=int(self.request.POST['diagn']))
		if self.request.POST.get('status-records', None):
			status = self.request.POST['status-records']
			if status == '0':
				st_list = [0]
			elif status == '1':
				st_list = [1]
			elif status == '2':
				st_list = [2, 3, 4]
			else:
				st_list = [0, 1, 2, 3, 4]
			params.update(status__in=st_list)
		if self.request.POST.get('medhome', None):
			params.update(medhome_id=int(self.request.POST['medhome']))

		d_start = self.request.POST['date-start'] if self.request.POST.get('date-start', None) else None
		d_end = self.request.POST['date-end'] if self.request.POST.get('date-end', None) else None
		key_date = 'created_at' if self.request.POST.get('type-date', None) == 'выдача' else 'date_rec'
		if d_start or d_end:
			if not d_start:
				#значит есть только верхний порог времени
				params['{}__lte'.format(key_date)] = datetime.datetime.strptime(d_end, '%m/%d/%Y') 
			else:
				date_lim = datetime.datetime.strptime(d_end, '%m/%d/%Y') if d_end else datetime.date.today()
				d_start = datetime.datetime.strptime(d_start, '%m/%d/%Y')
				params['{}__range'.format(key_date)] = (d_start, date_lim)
		return self.queryset.filter(**params)

	def get_queryset(self):
		self.queryset = super(ListingRecords, self).get_queryset().order_by('status', '-urgent', '-created_at')
		#self.queryset.order_by('-created_at')
		if self.request.POST.get('is_filter', None):
			qs = self.filter_by()
			return qs
		elif self.request.POST.get('is_search', None):
			qs = self.search_by_patient()
			return qs
		return self.queryset

	