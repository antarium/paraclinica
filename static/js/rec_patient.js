 function HideAllBlocks(){
    $('#choice-patient').addClass('hide_block');
    $('#create-new-patient').addClass('hide_block');
    $('#new-record-patient').addClass('hide_block');
}

function FillingPatientMap(data){
    $('#patient-name').text(data['fio']);
    $('#patient-passport').text(data['passport_ser'] + ' ' + data['passport_num']);
    $('#patient-kontakt').text(data['kontakt']);
    $('#patient-email').text(data['email']);
    $('#patient-address').text(data['address']);
    $('#patient-allow-newsletter').text(data['newsletter']);
    //заполняем форму изменения данных пациента
    fio = data['fio'].split(' ');
    var surname = fio[0], name = fio[1], lastname = '';
    if (fio[2] == 'undefind'){
        lastname = '';
    }else{
        lastname = fio[2];
    }
    $('#change-patient-surname').val(surname);
    $('#change-patient-name').val(name);
    $('#change-patient-lastname').val(lastname);
    $('#change-patient-passport').val(data['passport_ser']+data['passport_num']);
    $('#change-patient-address').val(data['address']);

    $('#change-patient-kontakt').val(data['kontakt']);
    $('#change-patient-email').val(data['email']);
    $('#change-patient-oms').val(data['oms']);
}

function RecordFullInfo(id){
    //показать полную информацию по записи на обследование
    var url = $('#url-get-patient').attr('data-url-record-info');
    var form_data = new FormData();
    form_data.append('csrfmiddlewaretoken', $('input[name="csrfmiddlewaretoken"]').val());
    form_data.append('record_id', id);
    xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    
    xhr.onload = function(e){
        if(this.status == 200){
            data = JSON.parse(this.response);
            $('#rec-survay').text(data['survay']);
            $('#rec-created_at').text(data['created_at']);
            $('#rec-doctor').text(data['doctor']);
            $('#rec-diagn').text(data['diagn']);
            $('#rec-about').text(data['about']);
            $('#rec-status').text(data['status']);
            $('#rec-daterec').text(data['daterec']);
            $('#rec-timerec').text(data['timerec']);
            $('#rec-medhome').text(data['medhome']);
            $('#rec-notice_call').text(data['notice_call']);
            $('#rec-notice_email').text(data['notice_email']);
        }else{
            console.log('Ошибка запроса')
        }
    }
    xhr.send(form_data)
}

function FillingRecords(data){
    //body-table-records
    data.forEach(function(item, i){
        var tegg = '';
        switch(item['status_id']){
            case 0: //ожидание
                tegg = '<tr class="success">';
                break;
            case 1: //запись утверждена
                tegg = '<tr class="success">';
                break;
            case 2: //обследование пройдено
                tegg = '<tr >';
                break;
            case 3: //в обследовании отказано
                tegg = '<tr class="danger">';
                break;
        }
        tegg = tegg + '<td>' + item['created_at'] + '</td>' +
                '<td>' + item['survay'] + '</td>' +
                '<td>' + item['status'] + '</td>' +
                '<td>' + 
                '<a href="javascript://" title="Печать направления"' + 
                ' onclick=RecordPrint(' + item['id'] + ')>'+
                '<i class="fa fa-print"></i></a> &nbsp;&nbsp;' +
                '<a href="javascript://" data-target="#recInfo" ' + 
                'title="Показать информацию о направлении"' +
                ' data-toggle="modal" onclick=RecordFullInfo(' + item['id'] + ')>'+
                '<i class="fa fa-info"></i></a></td>';
        tegg = tegg + '</tr>';
        $('#body-table-records').append(tegg);
    });
}
 
function ViewPatient(id){
    var url = $('#url-get-patient').attr('data-url');
    console.log(url);
    $('#url-get-patient').attr('data-patient', id);
    var form_data = new FormData();
    form_data.append('csrfmiddlewaretoken', $('input[name="csrfmiddlewaretoken"]').val());
    form_data.append('id', id);
    xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.onload = function(e){
        if(this.status == 200){
            data = JSON.parse(this.response);
            HideAllBlocks();
            $('#new-record-patient').removeClass('hide_block');
            $('#id-patient').attr("data-id-patient", id);
            //очистить таблицу записей (чистим ее только в том случае, если 
            //заново подцепляем данные пациента)
            $('#body-table-records').empty();
            FillingPatientMap(data['info']);
            FillingRecords(data['records']);
        }else{
            alert('Ошибка передачи данных');
        }
    }
    xhr.send(form_data);
    
}

function ClearDataNewPatient(){
    //очистить поля ввода для нового пациента
    $('input[name="passport"]').val("");
    $('input[name="fio"]').val("");
    $('input[name="birthday"]').val("");
    $('input[name="address"]').val("");
    $('input[name="kontakt"]').val("");
}

function FindPatient(url){
    HideAllBlocks()
    var form_data = new FormData();
    form_data.append('csrfmiddlewaretoken', $('input[name="csrfmiddlewaretoken"]').val());
    form_data.append('oms', $('input[name="find-oms"]').val());
    xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.onload = function(e){
        if(this.status == 200){
            data = JSON.parse(this.response);
            switch (data.length){
                case 0:
                    $('#create-new-patient').removeClass('hide_block');
                    $('input[name="oms"]').val($('input[name="find-oms"]').val());
                    ClearDataNewPatient();
                    break;
                case 1:
                    ViewPatient(data[0]['id'])
                    //FillingPatientMap(data[0]);
                    break;
                default:
                    $('#choice-patient').removeClass('hide_block');
                    $('#list-choice-patient').empty();
                    data.forEach(function(item, i){
                        var tegg = '<li><a href="javascript://" onclick="ViewPatient(' + 
                                    item['id'] + ')" title="омс: ' + item['oms'] + '">' 
                                    + item['fio'] + '</a></li>';
                        $('#list-choice-patient').append(tegg);
                    });
                    var tegg = '<li><a href="javascript://" onclick="ViewRegPatient()">' + 
                                'Зарегистрировать нового пациента</a></li>';
                    $('#list-choice-patient').append(tegg);
                    break;
            }   

        }else{
            console.log('Ошибка запроса')
        }
    }
    xhr.send(form_data);
}

function ViewRegPatient(){
    HideAllBlocks();
    $('#create-new-patient').removeClass('hide_block');
    $('input[name="oms"]').val($('input[name="find-oms"]').val());
}

function SetTypeSurvay(){
    //устанавливаем вид обследования и получаем обследования
    $('#processing-survay').removeClass('hide_block');
    var url = $('#url-get-patient').attr('data-url-survay-list');
    var form_data = new FormData();
    form_data.append('csrfmiddlewaretoken', $('input[name="csrfmiddlewaretoken"]').val());
    form_data.append('type_survay', $('#type-survay').val())
    xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.onload = function(e){
        if(this.status == 200){
            data = JSON.parse(this.response);
            $('#select-survay').empty();
            var tegg = '<option value="0" selected="selected">выбор обследования</option>';
            $('#select-survay').append(tegg);
            data.forEach(function(item, i){
                tegg = '<option value="' + item['id'] + '">' + item['name'] + '</option>';
                $('#select-survay').append(tegg);
            });
            $('#processing-survay').addClass('hide_block');
        }else{
            console.log('Ошибка запроса');
        }
    }
    xhr.send(form_data);
}

function CheckSurvay(){
    $('#processing-survay').removeClass('hide_block');
    var url = $('#url-get-patient').attr('data-url-check-survay');
    var form_data = new FormData();
    form_data.append('csrfmiddlewaretoken', $('input[name="csrfmiddlewaretoken"]').val());
    form_data.append('survay', $('#select-survay').val());
    form_data.append('patient', $('#url-get-patient').attr('data-patient'));
    xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.onload = function(e){
        if (this.status == 200){
            data = JSON.parse(this.response);
            switch (data['status_id']){
                case 0:
                    alert('направление уже выдано, статус - ожидание');
                    break;
                case 1:
                    alert('направление уже выдано, направление уже утверждено');
                    break;
                case 3:
                    alert('в выбранном обследовании ранее было отказано');
                    break;
            }
            $('#processing-survay').addClass('hide_block');
        }else{
            console.log('Ошибка запроса');
        }
    }   
    xhr.send(form_data);
}

function CheckDataNewPatient(){
    var count_err = 0;
    /*проводит валидацию данных, введенных врачом*/
    if ($('input[name="passport"]').val() == ''){
        ++count_err;
        $('input[name="passport"]').parent().addClass('has-error');
    }else{
        $('input[name="passport"]').parent().removeClass('has-error');        
    }
    if ($('input[name="fio"]').val() == ''){
        ++count_err;
        $('input[name="fio"]').parent().addClass('has-error');
    }else{
        $('input[name="fio"]').parent().removeClass('has-error');   
    }
    if ($('input[name="birthday"]').val() == ''){
        ++count_err;
        $('input[name="birthday"]').parent().addClass('has-error');
    }else{
        $('input[name="birthday"]').parent().removeClass('has-error');   
    }
    if ($('input[name="address"]').val() == ''){
        ++count_err;
        $('input[name="address"]').parent().addClass('has-error');
    }else{
        $('input[name="address"]').parent().removeClass('has-error');   
    }
    if ($('input[name="kontakt"]').val() == ''){
        ++count_err;
        $('input[name="kontakt"]').parent().addClass('has-error');
    }else{
        $('input[name="kontakt"]').parent().removeClass('has-error');   
    }
    if ($('input[name="oms"]').val() == ''){
        ++count_err;
        $('input[name="oms"]').parent().addClass('has-error');
    }else{
        $('input[name="oms"]').parent().removeClass('has-error');   
    }
    return count_err
}

function CheckNewRecord(){
    var count_err = 0;
    //проводим валидацию данных
    if($('select[name="type-survay"]').val() == '0'){
        ++count_err;
        $('select[name="type-survay"]').parent().addClass('has-error');
    }else{
        $('select[name="type-survay"]').parent().removeClass('has-error');
    }
    if($('select[name="survay"]').val() == '0'){
        ++count_err;
        $('select[name="survay"]').parent().addClass('has-error');
    }else{
        $('select[name="survay"]').parent().removeClass('has-error');
    }
    if($('select[name="diagn"]').val() == '0'){
        ++count_err;
        $('select[name="diagn"]').parent().addClass('has-error');
    }else{
        $('select[name="diagn"]').parent().removeClass('has-error');
    }
    if($('textarea[name="about-rec"]').val() == ''){
        ++count_err;
        $('textarea[name="about-rec"]').parent().addClass('has-error');
    }else{
        $('textarea[name="about-rec"]').parent().removeClass('has-error');
    }
    return count_err
}

function RegNewPatient(){
    var form_data = new FormData(document.forms.form_new_patient);
    xhr = new XMLHttpRequest();
    xhr.open(document.forms.form_new_patient.method, document.forms.form_new_patient.action, true);
    xhr.onload = function(e){
        switch(this.status){
            case 200:
                ViewPatient(parseInt(this.response));
                break;
            case 409:
                alert('Пациент с таким номером полиса или паспорта уже зарегистрирован!');
                ViewPatient(parseInt(this.response));
                break;
            case 400:
                //неверно заданы некоторые параметры:
                alert('Не верно заданы параметры: ' + this.response);
                break;
            default:
                console.log('Ошибка запроса');
                break;
        }
    }
    //перед отправкой данных, убедимся, что все заполнено
    count_err = CheckDataNewPatient();
    if(count_err == 0){
        xhr.send(form_data);    
    }else{
        alert('Не все данные заполнены корректно!');
    }
        
}

function ChangePatient(){
    var form_data = new FormData(document.forms.form_change_patient);
    xhr = new XMLHttpRequest();
    xhr.open(document.forms.form_change_patient.method, document.forms.form_change_patient.action, true);
    xhr.onload = function(e){
        if (this.status == 200){
            ViewPatient(parseInt(this.response));
            $('#close-change-patient').trigger('click');   
        }else{
            console.log('ошибка сервера');
        }
    }
    xhr.send(form_data); 
}

function NewRecord(){
    var form_data = new FormData(document.forms.form_new_rec);
    form_data.append('patient_id', $('#id-patient').attr("data-id-patient"));
    xhr = new XMLHttpRequest();
    xhr.open(document.forms.form_new_rec.method, document.forms.form_new_rec.action, true);
    xhr.onload = function(e){
        if (this.status == 200){
            data = JSON.parse(this.response);
            FillingRecords(data);
        }else{
            console.log('ошибка сервера');
        }
    }
    count_err = CheckNewRecord();
    if(count_err == 0){
        xhr.send(form_data); 
    }else{
        alert('Не все данные заполнены корректно!');
    }
}

function SetFilter(filter){
    //установить фильтр на лист направлений
    var form_data = new FormData();
    form_data.append('patient_id', $('#id-patient').attr("data-id-patient"));
    form_data.append('csrfmiddlewaretoken', $('input[name="csrfmiddlewaretoken"]').val());
    form_data.append('filter', filter);
    xhr = new XMLHttpRequest();
    xhr.open('POST', $('#actual-filter').attr('data-filter-url'), true);
    xhr.onload = function(e){
        if (this.status == 200){
            data = JSON.parse(this.response);
            $('#body-table-records').empty();
            FillingRecords(data);
        }else{
            console.log('ошибка сервера');
        }
    }
    xhr.send(form_data);
}




