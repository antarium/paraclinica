function ChoiceRecord(){
	//если выбраны какие-нибудь записи - отображаем панель изменения статусов
	var check_list = $('input[name="checkbox-record"]:checked');
	if (check_list.length > 0){
		$('#change-status-panel').removeClass('hide_block');
	}else{
		$('#change-status-panel').addClass('hide_block');
	}
}

function FillingMedhomeList(med_list, id_groupbox){
	//на вход получаем массив медучреждений и заполняем groupbox
	$(id_groupbox).empty();
	$.each(med_list, function(index, value){
		$('<option>', {
			'value': value['id'],
			'text': value['name'],
			on: {
				click: function(event){
					$('#count-quot').text('квот: ' + value['count_quot']);
					$('#address-medhome-aprove').text(value['address']);
				}
			}
		}).appendTo(id_groupbox)
	});
}

function DialogSendToHome(rec_id){
	//получить список мед.учреждений по record-id
	var url = $('#body-table-records').attr('data-url-aprove');
	var form_data = new FormData();
	form_data.append('csrfmiddlewaretoken', $('input[name="csrfmiddlewaretoken"]').val());
	form_data.append('record', rec_id);
	xhr = new XMLHttpRequest();
	xhr.open('POST', url, true);
	xhr.onload = function(e){
        if (this.status == 200){
        	var data = JSON.parse(this.response);
        	//заполяню groupbox
        	FillingMedhomeList(data, '#medhome-list-aprove');
        	//Заполнение диалогового окна для утверждения направления
			var patient_name = $('#patient-'+rec_id).text(),
				survay = $('#survay-'+rec_id).text(),
				justification = $('#justification-'+rec_id).text();
			$('#patient-name').text(patient_name);
			$('#survay-name').text(survay);
			$('#justification-text').text(justification);
			$('#record-id').attr('data-record-id', rec_id);
        }else{
            console.log('ошибка сервера');
        }
    }
    xhr.send(form_data); 
    $('#count-quot').text('');
    $('#address-medhome-aprove').text('');
}

function FillingTableRecords(records){
	$('#table-all-records').empty();
	$.each(records, function(index, value){
		$('<tr>', {
			'id': 'records-row-'+index,
			'class': value['style']
		}).appendTo('#table-all-records');
		$('<td>', {
			append: $('<div>', {
				'class': 'checkbox',
				append: $('<label>', {
					append: $('<input>', {
						'type': 'checkbox',
						'name': 'checkbox-record',
						'value': value['id'],
						on: {
							change: function(event){
								ChoiceRecord();
							}
						}
					})
				})
			})
		}).appendTo('#records-row-'+index);
		$('<td>', {text: value['patient']}).appendTo('#records-row-'+index);
		$('<td>', {text: value['survay']}).appendTo('#records-row-'+index);
		$('<td>', {text: value['diagn']}).appendTo('#records-row-'+index);
		$('<td>', {text: value['doctor']}).appendTo('#records-row-'+index);
		$('<td>', {text: value['created_at']}).appendTo('#records-row-'+index);
		$('<td>', {'id': 'row-'+index+'-rec-'+value['id']}).appendTo('#records-row-'+index);
		$('<td>', {text: value['daterec']}).appendTo('#records-row-'+index);
		$('<td>', {text: value['timerec']}).appendTo('#records-row-'+index);
		$('<td>', {text: value['notice_call']}).appendTo('#records-row-'+index);
		$('<td>', {text: value['notice_email']}).appendTo('#records-row-'+index);
		$('<td>', {text: value['urgent']}).appendTo('#records-row-'+index);
		$('<td>', {text: value['last_action']}).appendTo('#records-row-'+index);
		
		if(value['status'] == 'ожидание'){
			$('<a>', {
				'href': 'javascript://',
				'title': 'утвердить направление',
				'data-target': '#toMedHome',
				'data-toggle': 'modal',
				'id': 'send-to-medhome-' + value['id'],
				'text': value['status'],
				on:{
					click: function(event){
						DialogSendToHome(value['id']);
					}
				}
			}).appendTo('#row-'+index+'-rec-'+value['id']);
		}else{
			$('#row-'+index+'-rec-'+value['id']).text(value['status']);
		}
	});
}

function PagInfo(pag){
	$('#block-pagination').empty();
	var page_previous = '',
		page_next = '';
	if (pag['num_page'] == 1){
		$('<li>', {
			'id': 'link-previous',
			'class': 'paginate_button previous disabled',
			append: $('<a>', {'href': 'javascript://', 'text': 'Предыдущая'})
		}).appendTo('#block-pagination');
	}else{
		$('<li>', {
			'id': 'link-previous',
			'class': 'paginate_button previous',
			append: $('<a>', {
				'href': 'javascript://',
				'text': 'Предыдущая',
				on: {
					click: function(event){
						CurrentPage(pag['num_page']-1);
					}
				}
			})
		}).appendTo('#block-pagination');
	}

	for(i = 1; i <= pag['pages']; i++){
		if (i == pag['num_page']){
			$('<li>',{
				'class': 'paginate_button active',
				append: $('<a>', {'href': 'javascript://', 'text': i})
			}).appendTo('#block-pagination');
		}else{
			$('<li>',{
				'class': 'paginate_button',
				append: $('<a>', {
					'href': 'javascript://', 
					'text': i,
					on: {
						click: function(event){
							CurrentPage(i)
						}
					}
				})
			}).appendTo('#block-pagination');
		}
	}

	if (pag['num_page'] == pag['pages']){
		$('<li>', {
			'id': 'link-next',
			'class': 'paginate_button next disabled',
			append: $('<a>', {'href': 'javascript://', 'text': 'Следующая',})
		}).appendTo('#block-pagination');
	}else{
		$('<li>', {
			'id': 'link-next',
			'class': 'paginate_button next',
			append: $('<a>', {
				'href': 'javascript://',
				'text': 'Следующая',
				on: {
					click: function(event){
						CurrentPage(pag['num_page']+1);
					}
				}
			})
		}).appendTo('#block-pagination');
	}

}

function SetFilter(){
	//устанавливает фильтры и перерисовывает таблицу и панель пагинации согласно фильтрам
	$('#find-patient').val('');
	var form_data = new FormData(document.forms.forma_set_filter_records);
	form_data.append('is_filter', true);
    xhr = new XMLHttpRequest();
    xhr.open(document.forms.forma_set_filter_records.method, 
    		 document.forms.forma_set_filter_records.action, true);
    xhr.onload = function(e){
        if (this.status == 200){
        	var data = JSON.parse(this.response);
        	FillingTableRecords(data['records']);
        	PagInfo(data['pag']);
        }else{
            console.log('ошибка сервера');
        }
    }
    xhr.send(form_data); 
}

function SearchPatient(){
	var form_data = new FormData();
	form_data.append('is_search', true);
	form_data.append('search_text', $('#find-patient').val());
	form_data.append('csrfmiddlewaretoken', $('input[name="csrfmiddlewaretoken"]').val());
	xhr = new XMLHttpRequest();
	xhr.open(document.forms.forma_set_filter_records.method, 
    		 document.forms.forma_set_filter_records.action, true);
	xhr.onload = function(e){
        if (this.status == 200){
        	var data = JSON.parse(this.response);
        	FillingTableRecords(data['records']);
        	PagInfo(data['pag']);
        }else{
            console.log('ошибка сервера');
        }
    }
    xhr.send(form_data); 
}

function CurrentPage(num){
	//по переданному номеру страницы(пагинация) получаем записи и отрисовываем их в таблице
	console.log(num);
} 

function ApprovRecord(){
	var form_data = new FormData(document.forms.forma_aprove);
	form_data.append('rec_id', $('#record-id').attr('data-record-id'));
    xhr = new XMLHttpRequest();
    xhr.open(document.forms.forma_aprove.method, 
    		 document.forms.forma_aprove.action, true);
    xhr.onload = function(e){
        if (this.status == 200){
        	var a_id = '#send-to-medhome-' + this.response;
    		var new_jast = $('#justification-'+this.response).clone();
  			$(a_id).parent().empty().text('утверждено').append(new_jast);
    		$('#close-change-patient').trigger('click');
        }else{
            console.log('ошибка сервера');
        }
    }
    xhr.send(form_data); 
}

