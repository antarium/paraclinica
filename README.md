Paraclinika v 4.0 
Полностью переработанная версия старенькой программки внешняя параклиника
Запуск проекта:
- используется Python 3.x
- если используется virtualenv, то предварительно ставим виртуальное окружение: virtualenv -p python3.x --no-site-package <path>
- устанавливаем нужные пакеты: pip install -r requirements.txt
- запускаем проект python3.x manage.py runserver