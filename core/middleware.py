# -*- coding: utf-8 -*-
from django.utils.deprecation import MiddlewareMixin

class NavigatePanel(MiddlewareMixin):
	"""
	вытаскиваем профиль авторизованного пользователя и строим навигационное меню
	исходя из настроек профиля
	"""
	def process_template_response(self, request, response):
		if request.user:
			context = response.context_data
			try:
				modules_list = request.user.extensionuser.profile.modules.all()
				context['nav_panel'] = [dict(group=module.caption, icon=module.icon, pages=module.section.all().order_by('priority')) for module in modules_list]
			except:
				pass
		return response