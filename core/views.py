from django.shortcuts import render, redirect
from django.views.generic import TemplateView, View
from django.conf import settings
from django.contrib.auth import login, logout
from core.forms import AuthForm

# Create your views here.
"""
class IndexView(TemplateView):
	template_name = 'index.html'

	def get(self, request, *args, **kwargs):
		if not request.user.extensionuser.profile.start_page:
			return redirect('/lk/')
		url = request.user.extensionuser.profile.start_page.url
		return redirect(url)
"""
class IndexView(View):

	def get(self, request, *args, **kwargs):
		if not request.user.extensionuser.profile.start_page:
			return redirect('/lk/')
		url = request.user.extensionuser.profile.start_page.url
		return redirect(url)

class ProfileView(TemplateView):
	template_name = 'profile.html'

class LoginView(TemplateView):
	template_name = 'login.html'

	def post (self, request, *args, **kwargs):
		form = AuthForm(**request.POST.copy())
		user = form.auth_processing()
		if user and user.is_active:
			login(request, user)
			return redirect(settings.LOGIN_REDIRECT_URL)
		if not user.is_staff:
			kwargs.update({'error_access': 'Access_denied'})
		kwargs.update(form.error_list)
		kwargs.update({'login': request.POST.get('login', None)})
		return super(LoginView, self).get(request, *args, **kwargs)

	def get_context_data (self, **kwargs):
		context = super(LoginView, self).get_context_data(**kwargs)
		context['error_login'] = kwargs.get('error_login', None)
		context['error_password'] = kwargs.get('error_password', None)
		context['text_login'] = kwargs.get('login', None)
		context['error_access'] = kwargs.get('error_access', None)
		return context

class LogoutView(View):

	def get(self, request, *args, **kwargs):
		logout(request)
		return redirect(settings.LOGIN_REDIRECT_URL)



