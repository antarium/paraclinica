from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Section(models.Model):
    class Meta:
        db_table = 'sections'
        verbose_name = 'Страница'
        verbose_name_plural = 'Страницы'
    name = models.CharField(max_length=100)
    caption = models.CharField(max_length=100, default="")
    url = models.CharField(max_length=255, default='/')
    priority = models.PositiveSmallIntegerField(default=0)
    icon = models.CharField(max_length=50, blank=True, null=True, 
                            help_text='значение класса, например с fontawesome')

    def save(self, *args, **kwargs):
        self.caption = self.caption if self.caption else self.name
        super(Section, self).save(*args, **kwargs)

    def __repr__(self):
        return "Section: {}".format(self.name)

    def __str__(self):
        return self.name

class Modules(models.Model):
    class Meta:
        db_table = 'modules'
        verbose_name = 'Модуль'
        verbose_name_plural = 'Модули'
    name = models.CharField(max_length=100)
    caption = models.CharField(max_length=100, default="")
    section = models.ManyToManyField(Section)
    icon = models.CharField(max_length=50, blank=True, null=True,
                            help_text='значение класса, например с fontawesome')

    def save(self, *args, **kwargs):
        self.caption = self.caption if self.caption else self.name
        super(Modules, self).save(*args, **kwargs)

    def __repr__(self):
        return "Modules: {}".format(self.name)

    def __str__(self):
        return self.name

class ProfileUser(models.Model):
    class Meta:
        db_table = 'profiles'
        verbose_name = 'Профиль/специальность'
        verbose_name_plural = 'Профили/специальности'
    name = models.CharField(max_length=255)
    modules = models.ManyToManyField(Modules)
    start_page = models.ForeignKey(Section, blank=True, null=True)
    split_blocks_page = models.BooleanField(default=True, 
                        help_text='разбивать разделы в меню на блоки или выводить все одним блоком')

    def __repr__(self):
        return "Profile: {}".format(self.name)

    def __str__(self):
        return self.name

class StructUnit(models.Model):
    class Meta:
        db_table = 'struct_unit'
        verbose_name = 'Структурное подразделение'
        verbose_name_plural = 'Структурные подразделения'
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255, blank=True, null=True)

    def __repr__(self):
        return "Unit: {}".format(self.name)

    def __str__(self):
        return self.name

class ExtensionUser(models.Model):
    class Meta:
        db_table = 'ext_user'
        verbose_name = 'Расширение таблицы User'
        verbose_name_plural = 'Расширения таблицы User'
    user = models.OneToOneField(User)
    profile = models.ForeignKey(ProfileUser, blank=True, null=True)
    structure = models.ForeignKey(StructUnit, blank=True, null=True)

    def __repr__(self):
        return "Ext. User: {}".format(self.user.username)

    def __str__(self):
        return "{} {} ({})".format(self.user.first_name, self.user.last_name, self.user.username)

    def name(self):
        return "{} {}".format(self.user.first_name, self.user.last_name)


