from django import forms 
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.db.models import Q
from django.conf import settings
from core.models import ExtensionUser

class AuthForm(): 
	def __init__(self, **kwargs):
		self.login = kwargs.get('login', None)[0]
		self.password = kwargs.get('password', None)[0]
		self.error_list = {'error_login': None, 'error_password': None}

	def auth_processing(self):
		if self.login and self.password:
			user = User.objects.filter(username=self.login).first()
			if user:
				user = authenticate(username=user.username, password=self.password)
				self.error_list['error_password'] = None if user else 'неверный пароль'
				return user
			else:
				self.error_list['error_login'] = 'неверное имя пользователя'
		return None