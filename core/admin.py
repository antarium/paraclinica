from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from core.models import *

# Register your models here.

class ExtensionUserInline(admin.StackedInline):
    model = ExtensionUser
    can_delete = False
    vervose_name_plural = 'Расширения к User'

class UserAdmin(UserAdmin):
    inlines = (ExtensionUserInline, )

admin.site.unregister(User)
admin.site.register(User, UserAdmin)

@admin.register(ProfileUser)
class ProfileUserAdmin(admin.ModelAdmin):
    fields = ('name', 'modules', 'start_page', )

@admin.register(Modules)
class ModulesAdmin(admin.ModelAdmin):
    fields = ('name',  'section', 'icon', )

@admin.register(Section)
class SectionAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'caption', 'url', 'priority',)

@admin.register(StructUnit)
class UnitAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'address', )
