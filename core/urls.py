from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from core.views import *


#import api_login_required
urlpatterns = [
	url(r'^login/', LoginView.as_view(), name='login'),
    url(r'^logout/', LogoutView.as_view(), name = 'logout'),
	url(r'^lk/', login_required(ProfileView.as_view()), name='profile'),
	url(r'^', login_required(IndexView.as_view()), name='index'),
]
